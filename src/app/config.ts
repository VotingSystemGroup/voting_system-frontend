import {environment} from '../environments/environment';

export const appConfig = {
    signUp: environment.apiUrl + '/user/signup',
    login: environment.apiUrl + '/user/login',
    addVoting: environment.apiUrl + '/voting/add',
    getVoting: environment.apiUrl + '/voting/all'
};
