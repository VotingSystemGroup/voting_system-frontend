import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MaterialModule} from './modules/material.module';
import {MainComponent} from './components/main/main.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {UserModule} from './modules/user/user.module';
import {AdminModule} from './modules/admin/admin.module';
import {LoginComponent} from './components/login/login.component';
import {AdminLoginComponent} from './components/admin-login/admin-login.component';
import {JwtInterceptor} from './interceptors/jwt.interceptor';
import {AuthService} from './services/auth.service';
import {AdminGuard} from './guards/admin.guard';
import { ReportComponent } from './components/report/report.component';
import {UtilsService} from './services/utils.service';
import {MatProgressSpinnerModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
    declarations: [
        AppComponent,
        MainComponent,
        LoginComponent,
        AdminLoginComponent,
        ReportComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        MaterialModule,
        HttpClientModule,
        AdminModule,
        UserModule,
        MatProgressSpinnerModule,
        BrowserAnimationsModule
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
        AuthService,
        AdminGuard,
        UtilsService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
