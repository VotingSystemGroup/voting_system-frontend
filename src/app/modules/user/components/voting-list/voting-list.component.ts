import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {Vote} from '../../../../interfaces/vote';
import {Candidate} from '../../../../interfaces/candidate';
import {User} from '../../../../interfaces/user';
import {UtilsService} from '../../../../services/utils.service';
import {Parties} from '../../../../consts/parties';
import {MatSnackBar} from '@angular/material';

@Component({
    selector: 'app-voting-list',
    templateUrl: './voting-list.component.html',
    styleUrls: ['./voting-list.component.scss']
})
export class VotingListComponent implements OnInit {

    currentUser: User;
    votes: Vote[];
    selectedCandidate: { id: string, partyName: string };
    hasUserVoted: boolean;
    infoClosed: boolean;
    votesLoading: boolean;
    parties = Parties;

    constructor(private router: Router, private userService: UserService, private utilsService: UtilsService, private snackBar: MatSnackBar) {
    }

    ngOnInit() {
        this.currentUser = this.utilsService.getCurrentUser();
        this.hasUserVoted = !!localStorage.getItem('hasUserVoted');
        this.infoClosed = !!localStorage.getItem('infoClosed');
        setTimeout(() => {
            this.getVotes();
        });
    }

    redirectToReport(): void {
        this.router.navigate(['/report']);
    }

    getVotes(): void {
        this.votesLoading = true;
        this.userService.getVoteTypes(this.currentUser.region).subscribe(votes => {
            this.votesLoading = false;
            this.votes = votes;
        });
    }

    onInputSelect(candidate: Candidate, party): void {
        this.selectedCandidate = {id: candidate.id, partyName: party.name};
    }

    onVoteSubmit(): void {
        this.hasUserVoted = true;
        localStorage.setItem('hasUserVoted', '1');
        const vote = {
            person: {id: this.selectedCandidate.id || null},
            voteType: {id: this.parties.find(p => p.name === this.selectedCandidate.partyName).id}
        };

        this.userService.submitVote(vote).subscribe(() => {
            this.snackBar.open('Głos oddany pomyślnie!', 'Ok', {
                duration: 2000
            });
        });
    }

    onInfoClose(): void {
        localStorage.setItem('infoClosed', '1');
    }

}
