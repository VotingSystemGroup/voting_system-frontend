import { Injectable } from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Vote} from '../../../interfaces/vote';
import {map} from 'rxjs/operators';

const apiUrl = environment.apiUrl;

export const endpoints = {
  getParties: apiUrl + '/party/region/',
  addVote: apiUrl + '/votes'
};


@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  getVoteTypes(region: string): Observable<Vote[]> {
    return this.http.get<any>(endpoints.getParties + `${region}`).pipe(
        map(data => {
            return data.map(vote => {
                return {
                    name: vote.partyName,
                    candidates: vote.partyCandidates
                };
            });
        })
    );
    // return of(votes);
  }

  submitVote(vote): Observable<void> {
      return this.http.post<any>(endpoints.addVote, vote);
  }
}
