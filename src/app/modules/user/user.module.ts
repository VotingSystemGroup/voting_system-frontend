import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VotingListComponent} from './components/voting-list/voting-list.component';
import {UserRoutingModule} from './user-routing.module';
import {UserService} from './services/user.service';
import {MatProgressSpinnerModule} from '@angular/material';

@NgModule({
    declarations: [VotingListComponent],
    imports: [
        CommonModule,
        UserRoutingModule,
        MatProgressSpinnerModule
    ],
    providers: [
        UserService
    ]
})
export class UserModule {
}
