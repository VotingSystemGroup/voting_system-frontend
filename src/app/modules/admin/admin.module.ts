import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminPanelComponent} from './components/admin-panel/admin-panel.component';
import {AdminRoutingModule} from './admin-routing.module';
import {AdminService} from './services/admin.service';
import {ReactiveFormsModule} from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material';

@NgModule({
    declarations: [
        AdminPanelComponent,
    ],
    imports: [
        CommonModule,
        AdminRoutingModule,
        ReactiveFormsModule,
        MatProgressSpinnerModule
    ],
    providers: [
        AdminService
    ]
})
export class AdminModule {
}
