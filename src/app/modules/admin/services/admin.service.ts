import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {Candidate} from '../../../interfaces/candidate';
import {VoteType} from '../../../interfaces/vote-type';
import {Parties} from '../../../consts/parties';

const apiUrl = environment.apiUrl;

export const endpoints = {
    signUp: apiUrl + '/user/signup',
    login: apiUrl + '/user/login',
    addVoting: apiUrl + '/voting/add',
    getVoting: apiUrl + '/voting/all',
    getVoteTypes: apiUrl + '/votetype',
    getAllCandidates: apiUrl + '/candidate/all',
    addCandidate: apiUrl + '/candidate',
};

@Injectable()
export class AdminService {

    constructor(private http: HttpClient) {
    }

    addCandidate(candidate: Candidate): Observable<any> {
        return this.http.post(endpoints.addCandidate, candidate);
        // candidates.push(candidate);
        // return of(true);
    }

    getAllCandidates(): Observable<Candidate[]> {
        return this.http.get<any>(endpoints.getAllCandidates).pipe(
            map(data => {
                return data.map(candidate => {
                    return {
                        id: candidate.id,
                        firstName: candidate.firstName,
                        lastName: candidate.lastName,
                        pesel: candidate.pesel,
                        party: Parties.find(p => p.id === candidate.partyId).name,
                        listPosition: candidate.listPosition,
                        region: candidate.region
                    };
                });
            })
        );

        // return of(candidates);
    }

    getVoteTypes(): Observable<VoteType[]> {
        // return this.http.get<any>(endpoints.getVoteTypes).pipe(
        //     map(data => {
        //         return data.map(type => {
        //             return {
        //                 id: type.id,
        //                 regionId: type.region_id,
        //                  name: typeName,
        //                 startDate: type.start_date,
        //                 endDate: type.end_date
        //             };
        //         });
        //     })
        // );
        return of([]);
    }

}
