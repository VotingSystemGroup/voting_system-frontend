import {Component, OnInit} from '@angular/core';
import {Candidate} from '../../../../interfaces/candidate';
import {AdminService} from '../../services/admin.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UtilsService} from '../../../../services/utils.service';
import {VoteType} from '../../../../interfaces/vote-type';
import {Parties} from '../../../../consts/parties';
import {Regions} from '../../../../consts/regions';
import {MatSnackBar} from '@angular/material';

@Component({
    selector: 'app-admin-panel',
    templateUrl: './admin-panel.component.html',
    styleUrls: ['./admin-panel.component.scss']
})
export class AdminPanelComponent implements OnInit {

    addCandidateFormGroup: FormGroup;
    candidates: Candidate[];
    candidatesLoading: boolean;
    voteTypes: VoteType[];
    voteTypesLoading: boolean;
    parties = Parties;
    regions = Regions;

    constructor(private adminService: AdminService, private router: Router, private fb: FormBuilder, private utilsService: UtilsService
    , private snackBar: MatSnackBar) {
    }

    ngOnInit() {
        this.getVoteTypes();
        this.getCandidates();

        this.addCandidateFormGroup = this.fb.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            pesel: ['', Validators.required],
            party: ['', Validators.required],
            listPosition: ['', Validators.required],
            region: ['', Validators.required]
        });
    }

    redirectToReport(): void {
        this.router.navigate(['/report']);
    }

    onAddCandidate(): void {
        const candidateToAdd: Candidate = {
            firstName: this.addCandidateFormGroup.get('firstName').value,
            lastName: this.addCandidateFormGroup.get('lastName').value,
            pesel: this.addCandidateFormGroup.get('pesel').value,
            party: this.addCandidateFormGroup.get('party').value,
            listPosition: this.addCandidateFormGroup.get('listPosition').value,
            region: this.addCandidateFormGroup.get('region').value
        };

        this.addCandidateFormGroup.reset();

        this.adminService.addCandidate(candidateToAdd).subscribe(() => {
            this.getCandidates();
            this.snackBar.open('Pomyślnie dodano kandydata!', 'Ok', {
                duration: 2000
            });
        });
    }

    private getVoteTypes(): void {
        this.voteTypesLoading = true;
        this.adminService.getVoteTypes().subscribe(types => {
            this.voteTypesLoading = false;
            this.voteTypes = types;
        });
    }

    private getCandidates(): void {
        this.candidatesLoading = true;
        this.adminService.getAllCandidates().subscribe(candidates => {
            this.candidatesLoading = false;
            this.candidates = candidates;
        });
    }

}
