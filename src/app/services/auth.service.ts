import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Router} from '@angular/router';
import {appConfig} from '../config';
import {UserAuthData} from '../interfaces/user-auth-data';
import {AdminAuthData} from '../interfaces/admin-auth-data';
import {User} from '../interfaces/user';

@Injectable()
export class AuthService {

  constructor(
      private http: HttpClient,
      private router: Router
  ) {}

  loginAdmin(data: AdminAuthData): Observable<any> {
    return of(true);
  }

  loginUser(data: UserAuthData): Observable<any> {
    // return this.http.post<any>(appConfig.login,{ data });
    const userObject: User = {
      id: '1',
      firstName: 'Jan',
      lastName: 'Kowalski',
      pesel: '62012292239',
      idCardNumber: 'XWB046769',
      idCardExpDte: new Date('2022-01-13 00:00:00.000000'),
      lastIncomeValue: 35000,
      regionId: 33,
      city: 'Bolków',
      region: 'Dolnośląskie',
      isAdmin: false
    };
    return of(userObject);
  }

  logout(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    localStorage.removeItem('hasUserVoted');
    localStorage.removeItem('infoClosed');
    this.router.navigate(['/login']);
  }
}
