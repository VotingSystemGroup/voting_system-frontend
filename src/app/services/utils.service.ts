import {Injectable} from '@angular/core';
import {User} from '../interfaces/user';

@Injectable()
export class UtilsService {
    isUserAdmin(): boolean {
        return !!JSON.parse(localStorage.getItem('currentUser')) && JSON.parse(localStorage.getItem('currentUser')).isAdmin;
    }

    isUserAuthenticated(): boolean {
        return !!JSON.parse(localStorage.getItem('token'));
    }

    getCurrentUser(): User {
        return JSON.parse(localStorage.getItem('currentUser'));
    }

    generateUuid(): string {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
            let r = Math.random() * 16 | 0;
            let v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
}
