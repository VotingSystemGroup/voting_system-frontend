export const Parties = [{id: 1, name: 'Stronnictwo Demokratyczne'}, {id: 2, name: 'Polska Partia Socjalistyczna'}, {
    id: 3,
    name: 'Unia Europejskich Demokratów'
}, {id: 4, name: 'Unia Polityki Realnej'}, {id: 5, name: 'Polskie Stronnictwo Ludowe'}, {
    id: 6,
    name: 'Polska Wspólnota Narodowa'
}, {id: 7, name: 'Krajowa Partia Emerytów i Rencistów'}, {id: 8, name: 'Liga Polskich Rodzin'}, {
    id: 9,
    name: 'Narodowe Odrodzenie Polski'
}, {id: 10, name: 'Unia Pracy'}, {id: 11, name: 'Ruch Katolicko Narodowy'}, {
    id: 12,
    name: 'Sojusz Lewicy Demokratycznej'
}, {id: 13, name: 'Porozumienie Polskie'}, {id: 14, name: 'Przymierze Ludowo-Narodowe'}, {
    id: 15,
    name: 'Prawo i Sprawiedliwość'
}, {id: 16, name: 'Platforma Obywatelska Rzeczypospolitej Polskiej'}, {
    id: 17,
    name: 'Unia Polskich Ugrupowań Monarchistycznych'
}, {id: 18, name: 'Liga Obrony Suwerenności'}, {id: 19, name: 'Komunistyczna Partia Polski'}, {
    id: 20,
    name: 'Chrześcijańska Demokracja III Rzeczypospolitej Polskiej'
}, {id: 21, name: 'Partia Zieloni'}, {id: 22, name: 'Socjaldemokracja Polska'}, {id: 23, name: 'Stronnictwo Pracy'}, {
    id: 24,
    name: 'Stronnictwo Narodowe'
}, {id: 25, name: 'Organizacja Narodu Polskiego - Liga Polska'}, {
    id: 26,
    name: 'Stronnictwo Narodowe im. Dmowskiego Romana'
}, {id: 27, name: 'Wolność i Równość'}, {id: 28, name: 'Polski Ruch Monarchistyczny'}, {
    id: 29,
    name: 'Stronnictwo Polska Racja Stanu'
}, {id: 30, name: 'II Rzeczpospolita Polska'}, {id: 31, name: 'Związek Słowiański'}, {
    id: 32,
    name: 'Obrona Narodu Polskiego'
}, {id: 33, name: 'Inicjatywa Feministyczna'}, {id: 34, name: 'Prawica Rzeczypospolitej'}, {
    id: 35,
    name: 'Stronnictwo „Piast"'
}, {id: 36, name: 'Polska Lewica'}, {id: 37, name: 'Europa Wolnych Ojczyzn - Partia Polska'}, {
    id: 38,
    name: 'Polska Patriotyczna'
}, {id: 39, name: 'Samoobrona'}, {id: 40, name: 'Lepsza Polska'}, {id: 41, name: 'Kongres Nowej Prawicy'}, {
    id: 42,
    name: 'Partia Rozwoju'
}, {id: 43, name: 'Twój Ruch'}, {id: 44, name: 'Stronnictwo Patriotyczne Polski i Polonii'}, {
    id: 45,
    name: 'Solidarna Polska'
}, {id: 46, name: 'Demokracja Bezpośrednia'}, {id: 47, name: 'Stronnictwo Ludowe "Ojcowizna" RP'}, {
    id: 48,
    name: 'Polska Partia Piratów'
}, {id: 49, name: 'Polskie Stronnictwo Demokratyczne'}, {id: 50, name: 'Liga Narodowa'}, {
    id: 51,
    name: 'Porozumienie Jarosława Gowina'
}, {id: 52, name: 'Ruch Sprawiedliwości Społecznej'}, {id: 53, name: 'Przedsiębiorcza Rzeczypospolita Polska'}, {
    id: 54,
    name: 'Normalny Kraj'
}, {id: 55, name: 'Piast – Jedność Myśli Europejskich Narodów'}, {id: 56, name: 'Lewica Razem'}, {
    id: 57,
    name: 'Partia Wolności'
}, {id: 58, name: 'Zjednoczenie Chrześcijańskich Rodzin'}, {id: 59, name: 'Nowoczesna'}, {
    id: 60,
    name: 'Wolni i Solidarni'
}, {id: 61, name: 'Partia Chłopska'}, {id: 62, name: 'Ruch Narodowy'}, {id: 63, name: 'Jeden-PL'}, {
    id: 64,
    name: 'Liga Samorządowa Pierwsza'
}, {id: 65, name: 'Regionalna. Mniejszość z Większością'}, {id: 66, name: 'Ślonzoki Razem'}, {
    id: 67,
    name: 'Śląska Partia Regionalna'
}, {id: 68, name: 'Inicjatywa Obywatelska'}, {id: 69, name: 'Ruch 11 Listopada'}, {
    id: 70,
    name: 'Akcja Zawiedzionych Emerytów Rencistów'
}, {id: 71, name: 'Wiosna Roberta Biedronia'}, {id: 72, name: 'Alternatywa Społeczna'}, {
    id: 73,
    name: 'Jedność Narodu'
}, {id: 74, name: 'Partia Kierowców'}, {id: 75, name: 'Ruch Prawdziwa Europa – Europa Christi'}, {
    id: 76,
    name: 'Skuteczni Piotra Liroya-Marca'
}, {id: 77, name: 'Zgoda'}, {id: 78, name: 'Odpowiedzialność'}, {id: 79, name: 'Polska Partia Internetowa'}, {
    id: 80,
    name: 'Konfederacja Wolność i Niepodległość'
}, {id: 81, name: 'Inicjatywa Polska'}, {id: 82, name: 'Polska Nas Potrzebuje'}, {
    id: 83,
    name: 'Konfederacja – Koalicja Propolska'
}, {id: 84, name: 'Sami Swoi'}];
