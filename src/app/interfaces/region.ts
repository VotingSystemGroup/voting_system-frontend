export interface Region {
    id: number;
    voivodeshipCode: number;
    countCode: number;
    voivodeship: string;
    count: string;
    commune: string;
}
