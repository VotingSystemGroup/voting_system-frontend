export interface User {
    id: string;
    firstName: string;
    lastName: string;
    pesel: string;
    idCardNumber: string;
    idCardExpDte: Date;
    lastIncomeValue: number;
    regionId: number;
    city: string;
    region: string;
    isAdmin: boolean;
}
