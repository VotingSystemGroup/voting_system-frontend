import {Candidate} from './candidate';

export interface Vote {
    name: string;
    candidates: Candidate[];
}
