import {Candidate} from './candidate';

export interface Party {
    id?: string;
    name: string;
    candidates?: Candidate[];
}
