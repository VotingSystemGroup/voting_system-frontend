export interface VoteType {
    id: string;
    regionId: string;
    name: string;
    startDate: Date;
    endDate: Date;
}
