export interface Candidate {
    id?: string;
    firstName: string;
    lastName: string;
    pesel?: number;
    party?: string;
    listPosition?: number;
    region?: string;
}
