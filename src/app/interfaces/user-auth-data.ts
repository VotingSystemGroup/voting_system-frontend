export interface UserAuthData {
  firstName: string;
  lastName: string;
  pesel: string;
  idCardNumber: string;
  lastIncomeValue: string;
}
