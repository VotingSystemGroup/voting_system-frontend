export interface AdminAuthData {
    login: string;
    password: string;
}
