import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainComponent} from './components/main/main.component';
import {SessionGuard} from './guards/session.guard';
import {AdminLoginComponent} from './components/admin-login/admin-login.component';
import {LoginComponent} from './components/login/login.component';
import {AdminGuard} from './guards/admin.guard';
import {UserGuard} from './guards/user.guard';
import {ReportComponent} from './components/report/report.component';

const routes: Routes = [
    {
        path: '',
        component: MainComponent,
        canActivate: [SessionGuard],
        children: [
            {
              path: 'report',
              component: ReportComponent
            },
            {
                path: '',
                canActivate: [UserGuard],
                // @ts-ignore
                loadChildren: () => import('./modules/user/user.module').then(m => m.UserModule)
            },
            {
                path: 'admin',
                canActivate: [AdminGuard],
                // @ts-ignore
                loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule)
            }
        ]
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'admin-login',
        component: AdminLoginComponent
    },
    {
        path: '**',
        redirectTo: 'login'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
