import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {UserAuthData} from '../../interfaces/user-auth-data';
import {UtilsService} from '../../services/utils.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginInprogress = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private utilsService: UtilsService) {
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      pesel: ['', Validators.required],
      lastIncomeValue: ['', Validators.required],
      idCardNumber: ['', Validators.required],
      rulesCheckbox: ['', Validators.required]
    });
  }

  onAdminLoginClick(): void {
    this.router.navigate(['admin-login']);
  }

  onSubmit(): void {
      const userToLogin: UserAuthData = {
        firstName: this.loginForm.get('firstName').value,
        lastName: this.loginForm.get('lastName').value,
        pesel: this.loginForm.get('pesel').value,
        lastIncomeValue: this.loginForm.get('lastIncomeValue').value,
        idCardNumber: this.loginForm.get('idCardNumber').value
      };
      this.loginInprogress = true;
      this.authService.loginUser(userToLogin).subscribe((data) => {
        this.loginInprogress = false;
        localStorage.setItem('token', JSON.stringify(data.token));
        localStorage.setItem('currentUser', JSON.stringify(data));
        this.router.navigate(['']);
      }, () => {
        this.loginInprogress = false;
      });
  }
}
